public class Student{
	
	int number; 
	String program;
	String school;
	
	public Student(int n, String p, String s){ 
		number = n;
		program = p;
		school = s;
	}
	
	public String welcomeMessage(){
		return "Hello student " + number + "! Welcome to the " + program + " program.";
	}
	
	public String schoolEnrolled(){
		if ("Dawson".equals(school)){
			return " I hope you enjoy your stay in Dawson college! I would be your tour guide until you're familiar with the environment";
		}
		else {
			return " I hope you enjoy your stay in " + school + " college!";
		}
	}
}
