public class Application{
	
	public static void main(String[] args){
		Student numA = new Student(2338949, "Computer Science", "Dawson");
		Student numB = new Student(1234567, "Arts", "Vanier");
			//System.out.println(numA.welcomeMessage() + numA.schoolEnrolled());
			//System.out.println(numB.welcomeMessage() + numB.schoolEnrolled());
			
		Student[] section3 = new Student[3];
		section3[0] = numA;
		section3[1] = numB;
		section3[2] = new Student(5678901, "Law and Justice", "Dawson");
		System.out.println(section3[0].welcomeMessage());
		System.out.println(section3[0].schoolEnrolled());
		System.out.println(section3[1].welcomeMessage());
		System.out.println(section3[1].schoolEnrolled());
		System.out.println(section3[2].welcomeMessage());
		System.out.println(section3[2].schoolEnrolled());

	} 
}